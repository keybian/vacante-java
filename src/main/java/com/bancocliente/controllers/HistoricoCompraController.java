package com.bancocliente.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bancocliente.dto.HistoricoCreacionCompraDto;
import com.bancocliente.services.HistoricoCompraService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Padilla
 *
 */
@RestController
@Api(value = "His_Compra", consumes = "application/json")
@RequestMapping("HistoricoCompras")
public class HistoricoCompraController {
private static final String URL_CREATION = "/HistoricoCompras/{id-invgrupo}";
	
//	@Autowired
//	Validador Validador;
	
	@Autowired
	HistoricoCompraService historicoCompraService;

	/** Servico para crear los grupos de inventario
	 * 
	 * @param invGrupoCreacionDto objeto para la creacion de un grupos de inventario
	 * @return url del grupo de inventario  Creado
	 * @throws Exepcion : retorna codigo y mensaje de error
	 * 
	 */
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	@ApiOperation(value = "Crear Grupos del Inventario", notes = "Crear Grupos del Inventario")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Grupo Inventario creada Satisfactoriamente"),
			@ApiResponse(code = 400, message = "Excepcion controlada, durante el proceso"),
			@ApiResponse(code = 500, message = "Excepcion Tecnica generada durante el proceso")})
	public ResponseEntity<Void> crearInvGrupo(@RequestBody HistoricoCreacionCompraDto historicoCreacionCompraDto)
			throws RuntimeException
	{
		
		//Control de Presencia
		
		
		//Control de Forma
		
		
		
		
		//Control de Coherencia
		
		
		
		//Control de Fondo
		
		
		
		
		
		Long idHistorico =historicoCompraService.crearCompra(historicoCreacionCompraDto);
		
	    URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
	    		 .path(URL_CREATION)
	    		 .buildAndExpand(idHistorico)
	    		 .toUri();
	     	return ResponseEntity.created(location).build();
	}
	
	
	
	

}
