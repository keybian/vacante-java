package com.bancocliente.services;

import com.bancocliente.dto.HistoricoCreacionCompraDto;

public interface HistoricoCompraService {
	
	Long crearCompra(HistoricoCreacionCompraDto historicoCreacionCompraDto) throws RuntimeException;

}
