package com.bancocliente.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancocliente.services.HistoricoCompraService;
import com.bancocliente.entitys.HistoricoCuentas;
import com.bancocliente.entitys.repositorios.HistoricoRepositorio;
import com.bancocliente.dto.HistoricoCreacionCompraDto;
import com.bancocliente.validation.Validar;

/**
 * @author Padilla
 *
 */
@Service
public class HistoricoCompraServiceImpl implements HistoricoCompraService {
	
	@Autowired
	Validar validar;
	
	@Autowired
	HistoricoRepositorio historicoRepositorio;

	@Override
	public Long crearCompra(HistoricoCreacionCompraDto historicoCreacionCompraDto) throws RuntimeException {
		// TODO Auto-generated method stub
		
		HistoricoCuentas  historicoCuentas = HistoricoCuentas.builder()
				.idCliHis(historicoCreacionCompraDto.getIdCli())
				.idProHis(historicoCreacionCompraDto.getIdPro())
				.compraHis(historicoCreacionCompraDto.getCompra())
				.desHis(historicoCreacionCompraDto.getDescripcion())
				.recargaHis(0L)
				.build();
		historicoRepositorio.save(historicoCuentas);
		return historicoCuentas.getCompraHis();
	}
	

}
