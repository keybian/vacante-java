package com.bancocliente.dto;

import io.swagger.annotations.ApiModelProperty;

public class HistoricoCreacionRecargaDto implements java.io.Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6441538116126398915L;
	
	@ApiModelProperty
	private Long idCli;
	
	@ApiModelProperty
	private Long idPro;
	
	
	@ApiModelProperty
	private String descripcion;
	
	@ApiModelProperty
	private Long recarga;
	
}
