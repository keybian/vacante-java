package com.bancocliente.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;;

@Getter

@Setter

@NoArgsConstructor

@Builder

@AllArgsConstructor

@Entity

@Table(name="producto")

public class Producto implements java.io.Serializable{
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1685661087079810164L;

	@Id
	@Column(name= "PPRO_ID", unique = true, precision = 10 , scale = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPro;
	
	@Column(name = "PRO_CODIGO", nullable = false, length = 10, scale = 0)
	private String codigoPro;
	
	
	@Column(name = "PRO_NOMBRE",nullable = false,length = 70,scale=0)
	private String nombrePro;
	

	
	@Column(name = "CLI_SITUAC",length = 1,scale=0)
	private String situacCli;
	

}
