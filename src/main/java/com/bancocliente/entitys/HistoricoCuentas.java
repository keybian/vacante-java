package com.bancocliente.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;;

@Getter

@Setter

@NoArgsConstructor

@Builder

@AllArgsConstructor

@Entity

@Table(name="historico")

public class HistoricoCuentas implements java.io.Serializable{
	
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1666745132033668676L;

	/** id Historico */
	@Id
	@Column(name = "HIS_ID", unique = true, nullable = false,precision = 10, scale =0)
	//@SequenceGenerator(name = "AreaCriticagen", sequenceName = "AreaCritica_seq")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idHis;
	
	/** Cliente. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLI_ID", nullable = false, insertable = false, updatable = false)
    private Cliente cliente;
    
    @Column(name="CLI_ID")
    private  Long idCliHis;
    
    /** Producto. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRO_ID", nullable = false, insertable = false, updatable = false)
    private Producto producto;
    
    @Column(name="PRO_ID")
    private  Long idProHis;
    
    @Column(name="PRO_DES")
    private  String desHis;
    
    
    /** Compra */
	
	@Column(name = "HIS_COM")
	private Long compraHis;
	
	/** Recargar */
	@Column(name = "HIS_REC")
	private Long recargaHis;
	
	
	

}
