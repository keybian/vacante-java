package com.bancocliente.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;;

@Getter

@Setter

@NoArgsConstructor

@Builder

@AllArgsConstructor

@Entity

@Table(name="cliente")

public class Cliente implements java.io.Serializable{
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3553372738931513851L;

	@Id
	@Column(name= "CLI_ID", unique = true, precision = 10 , scale = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCli;
	
	@Column(name = "CLI_CODIGO", nullable = false, length = 10, scale = 0)
	private String codigoCli;
	
	
	@Column(name = "CLI_NOMBRE",nullable = false,length = 70,scale=0)
	private String nombreCli;
	
	@Column(name = "CLI_APELLIDO",nullable = false,length = 70,scale=0)
	private String apellidoCli;
	

	
	@Column(name = "CLI_SITUAC",length = 1,scale=0)
	private String situacCli;
	

}
